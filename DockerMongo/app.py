import os
from flask import Flask, redirect, url_for, request, render_template, jsonify
from pymongo import MongoClient
import arrow
import acp_times
import config
import logging

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


#The environment of mongoDB
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

#Use to link with DB
db = client.tododb
#clear database
db.tododb.delete_many({})  
#from proj4

@app.route('/')
@app.route('/index')
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    
    return render_template('404.html'), 404





#base on project4 because I have bugs for p4, I have to rewrite some logic to make sure it can be used
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist= request.args.get('dis', 999, type=float)
    begintime = request.args.get('bt', 999, type=str)
    begindate = request.args.get('bd', 999, type=str)
    timeformat = "{}T{}".format(begindate, begintime)
    timing = arrow.get(timeformat)
#remove error part from p4
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, dist, timing.isoformat())
    close_time = acp_times.close_time(km, dist, timing.isoformat())

    rslt = {"open": open_time, "close": close_time}
    
    return jsonify(result=rslt)


@app.route('/todo', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    #return nothing for webpage, if have data, go to todo.html
    if items == []:
        return render_template('nothing.html')
    else:
        return render_template('todo.html', items=items)

@app.route('/nothing')
def nothing():
    return render_template('nothing.html')


@app.route('/new', methods=['POST'])
def new():
    #get data
    opentimedata = request.form.getlist("open")
    closetimedata = request.form.getlist("close")
    kmdata = request.form.getlist("km")
    miledata =request.form.getlist("miles")
#save data
    open_times = [x for x in opentimedata if x != '']
    close_times = [w for w in closetimedata if w != '']
    kmlen = [y for y in kmdata if y != '']
    milelen = [z for z in miledata if z !='']

    
    list_length = len(open_times)
    for i in range(list_length):
        item_doc = {
            'km' : kmlen[i],
            'miles':milelen[i],
            'open_time': open_times[i],
            'close_time': close_times[i]
        }
        db.tododb.insert_one(item_doc)

     #use for check error for all database
    _items = db.tododb.find()
    items = [item for item in _items]
    
    #if nothing in data,go to nothing rather than go to index
    if items == []:
        return redirect(url_for('nothing'))
    else:
        return redirect(url_for('index'))



#############

#Added from proj4
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
